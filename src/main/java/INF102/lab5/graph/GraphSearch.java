package INF102.lab5.graph;

import java.util.HashSet;
import java.util.Set;
/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;
    private Set<V> used = new HashSet<>();

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }


    @Override
    public boolean connected(V u, V v) {
        if(graph.hasNode(u) && graph.hasNode(v)){
            used.add(u);
            if (graph.hasEdge(u, v)){
                return true;
            }
            if(graph.getNeighbourhood(u) != null){
                Set<V> neighbors = graph.getNeighbourhood(u);
                if(!neighbors.isEmpty()){
                    for(V node : neighbors){
                        if(!used.contains(node)){
                            return connected(node, v);
                        }
                    }
                }
            }
        }
        return false;
    }

}
