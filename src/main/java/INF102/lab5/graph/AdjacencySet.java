package INF102.lab5.graph;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdjacencySet<V> implements IGraph<V> {

    private Set<V> nodes;
    private Map<V, Set<V>> nodeToNode;

    public AdjacencySet() {
        nodes = new HashSet<>();
        nodeToNode = new HashMap<>();
    }

    @Override
    public int size() {
        return nodes.size();
    }

    @Override
    public Set<V> getNodes() {
        return Collections.unmodifiableSet(nodes);
    }

    @Override
    public void addNode(V node) {
        nodes.add(node);
    }

    @Override
    public void removeNode(V node) {
        nodes.remove(node);
        nodeToNode.remove(node);
    }

    @Override
    public void addEdge(V u, V v) {
        if (nodes.contains(u) && nodes.contains(v)){
            if (!nodeToNode.containsKey(u)){
                nodeToNode.put(u, new HashSet<>());
                nodeToNode.get(u).add(v);
            }
            else{
                nodeToNode.get(u).add(v);
            }
            if (!nodeToNode.containsKey(v)){
                nodeToNode.put(v, new HashSet<>());
                nodeToNode.get(v).add(u);
            }
            else{
                nodeToNode.get(v).add(u);
            }

        }
        else{
            throw new IllegalArgumentException("Cant add egde to a node that does not exist");
        }
    }

    @Override
    public void removeEdge(V u, V v) {
        if(nodeToNode.containsKey(u) && nodeToNode.containsKey(v)){
            nodeToNode.get(u).remove(v);
            nodeToNode.get(v).remove(u);
        }
    }

    @Override
    public boolean hasNode(V node) {
        return nodes.contains(node);
    }

    @Override
    public boolean hasEdge(V u, V v) {
        if(nodeToNode.containsKey(u)){
            return nodeToNode.get(u).contains(v);
        }
        return false;

    }

    @Override
    public Set<V> getNeighbourhood(V node) {
        if (nodeToNode.containsKey(node)){
            return Collections.unmodifiableSet(nodeToNode.get(node));
        }
        else return null;

    }

    @Override
    public String toString() {
        StringBuilder build = new StringBuilder();
        for (V node : nodeToNode.keySet()) {
            Set<V> nodeList = nodeToNode.get(node);

            build.append(node);
            build.append(" --> ");
            build.append(nodeList);
            build.append("\n");
        }
        return build.toString();
    }

}
